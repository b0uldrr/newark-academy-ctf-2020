#!/usr/bin/python3

# Open the enc.txt file and read in the big long line of 1's and 0's
f = open("enc.txt", "r")
line = f.read()

# Split that line into chunks of 8 bits.
# Convert those ascii 1's and 0's into actual binary values.
# Then convert those binary values into their ascii equivalent.
char_list = []
for i in range(0, len(line), 8):
    char_list.append(chr(int(line[i:i+8],2)))

# We currently have a list of ascii characters 2929 long. We know the transmission 
# was sent 101 times, which means that the transmission is 29 characters long.
# Split our long list into 101 lists of 29 characters.
word_list = []
for i in range(0, len(char_list), 29):
    word_list.append(char_list[i:i+29])

# Go through each position in the message, 0-28
for i in range(0, 29):
    chars = {}

    # Maintain a dictionary with every different character found at that position,
    # along with a count of the number of times it was found.
    for tx in word_list:
        if(tx[i] in chars.keys()):
            chars[tx[i]] = chars[tx[i]] + 1
        else:
            chars[tx[i]] = 0
   
   # Now go through the dictionary and find the letter with the highest count.
   # That's our winner. Print it out.
    highest_count = 0
    for c in chars:
        if(chars[c] > highest_count):
            winner = c
            highest_count = chars[c]

    print(winner,end='')
