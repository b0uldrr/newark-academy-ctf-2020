## Error 0

* **CTF:** Newark Academy CTF 2020
* **Category:** Crypto 
* **Points:** 150
* **Solved By:** b0uldrr
* **Date:** 1/11/20

---

### Challenge
```
Rahul has been trying to send a message to me through a really noisy communication channel. Repeating the message 101 times should do the trick!

-izhang05

```

---

### Downloads
* [enc.txt](enc.txt)

---

### Solution
`enc.txt` is long line of 1's and 0's. My solution was to peform a frequncy analysis for the character found at each position of the message and then accept the most commonly found character.

1. Open `enc.txt` in python, read in the line.
1. Split the line up into 8 character "bytes", which are still strings at this stage
1. Convert those strings into actual bytes and their char representation
1. We know from the challenge text that the message was sent 101 times. By checking the length of our characters, we can see we have 2929 of them. 2929/101 = 29, which means the message length is 29 characters.
1. Check character position 0 for each message and create a dictionary for every character found in that position, along with a count of the number of times it is found.
1. After checking all 101 messages, check to see which character was found the most in that position. This is probably the correct character. Append it to the flag.
1. Repeat steps 5-6 for the other 28 positions in each message.
1. Print the flag.

```python
#!/usr/bin/python3

# Open the enc.txt file and read in the big long line of 1's and 0's
f = open("enc.txt", "r")
line = f.read()

# Split that line into chunks of 8 bits.
# Convert those ascii 1's and 0's into actual binary values.
# Then convert those binary values into their ascii equivalent.
char_list = []
for i in range(0, len(line), 8):
    char_list.append(chr(int(line[i:i+8],2)))

# We currently have a list of ascii characters 2929 long. We know the transmission 
# was sent 101 times, which means that the transmission is 29 characters long.
# Split our long list into 101 lists of 29 characters.
word_list = []
for i in range(0, len(char_list), 29):
    word_list.append(char_list[i:i+29])

# Go through each position in the message, 0-28
for i in range(0, 29):
    chars = {}

    # Maintain a dictionary with every different character found at that position,
    # along with a count of the number of times it was found.
    for tx in word_list:
        if(tx[i] in chars.keys()):
            chars[tx[i]] = chars[tx[i]] + 1
        else:
            chars[tx[i]] = 0
   
   # Now go through the dictionary and find the letter with the highest count.
   # That's our winner. Print it out.
    highest_count = 0
    for c in chars:
        if(chars[c] > highest_count):
            winner = c
            highest_count = chars[c]

    print(winner,end='')
```

![flag](images/flag.png)

---

### Flag 
```
nactf{n01sy_n013j_|\|()|$'/}
```
