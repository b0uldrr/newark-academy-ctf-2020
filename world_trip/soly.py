#!/usr/bin/python3
from geopy.geocoders import Nominatim

f = open("enc.txt", "r")
line = f.readline()

# Remove first "(" and last ")"
line = line[1:]
line = line[:-1]

# Split by separating ")("
coords = line.split(')(')

# Need to specify a user agent to use the API
geolocator = Nominatim(user_agent="ctf")

flag = ""

for c in coords:

    # Reverse lookup of coordinates. Note that the language='en' is important or otherwise
    # each country will be referred to by its local name it its local language.
    location = geolocator.reverse(c, language='en')

    # Returned location is in format "Suburb, City, State, Country", or similiar. It can vary,
    # but all parts are separated by commas, and the country is always last. Split the location
    # so that we can address in the individual parts.
    address = location.address.split(',')
    
    # Get the country from the end of the address list and then only print the first character
    flag += address[-1][1]
    print(flag)

print()
