## World Trip

* **CTF:** Newark Academy CTF 2020
* **Category:** General Skills 
* **Points:** 300
* **Author(s):** b0uldrr
* **Date:** 1/11/20

---

### Challenge
```
Will has been travelling the world! Here's a list of the coordinates of the places he's been to. There might be a secret message hidden in the first letter of all of the countries.

Note: The flag is all uppercase without spaces or "_"

-izhang05

```
---

### Download
* [enc.txt](enc.txt)

---

### Hints
```
This would be really tedious to do by hand...
```
```
The last part of the flag is just random characters
```
---

### Solution

The provided file `enc.txt` is a long list of coordinates. The challenge description is very clear; we need to convert these coordinates to the countries they belong to and take the first letter of every country. I made a python script to automate this and used the [Geopy](https://pypi.org/project/geopy/) library to perform reverse lookup of the GPS coordinates supplied in `enc.txt`.

```python
#!/usr/bin/python3
from geopy.geocoders import Nominatim

f = open("enc.txt", "r")
line = f.readline()

# Remove first "(" and last ")"
line = line[1:]
line = line[:-1]

# Split by separating ")("
coords = line.split(')(')

# Need to specify a user agent to use the API
geolocator = Nominatim(user_agent="ctf")

for c in coords:

    # Reverse lookup of coordinates. Note that the language='en' is important or otherwise
    # each country will be referred to by its local name it its local language.
    location = geolocator.reverse(c, language='en')

    # Returned location is in format "Suburb, City, State, Country", or similiar. It can vary,
    # but all parts are separated by commas, and the country is always last. Split the location
    # so that we can address in the individual parts.
    address = location.address.split(',')
    
    # Get the country from the end of the address list and then only print the first character
    print(address[-1][1],end='')

print()
```

![flag](images/flag.png)

---

### Flag 
```
nactf{IHOPEYOUENJOYEDGOINGONTHATREALLYLONGGLOBALTOURIBOFAIQFUSETZOROPZNQTLENFLFSEMOGMHDBEEIZOIUOCGSLCDYMQYIRLBZKNHHFGBPDIVNBUQQYPDCQIAVDYTRFOCESEQUOUUMSKYJOVKVJGMRGNATNIRESHRKHCEDHHZYQRZVOGCHSBAYUBTRU}
```
