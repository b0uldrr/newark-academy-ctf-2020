## Patches

* **CTF:** Newark Academy CTF 2020
* **Category:** Rev
* **Points:** 150 
* **Author(s):** b0uldrr
* **Date:** 31/10/20

---

### Challenge
```
This binary does nothing.

-asphyxia

```
---

### Download
* [patches](patches) 

---

### Solution

*Note: The following solution works and I believe is the intented way to solve this challenge... However in reading other writeups, some people ran the binary in GDB and then changed the value of $RIP to jump straight to `print_flag()`. Check them out too.*

Running the provided binary, it just prints "Goodbye" to the screen and then exits. I decompiled it in Ghidra. The `main` function is programmed exactly as expected, with a single call to `puts` and before returning. 

![main](images/main.png)

There were 2 other interesting functions. `print_flag`, and then another unlabelled function which is called by `print_flag`:
![printflag](images/print_flag.png)
![func](images/func.png)

Given the name of the challenge, our goal is to patch the binary to divert the flow of the program to run `print_flag`.

You need to reimport the program as a raw binary to be able to patch it in Ghidra, otherwise your patched program will throw a segmantation fault. I followed [this tutorial](https://www.youtube.com/watch?v=8U6JOQnOOkg). In short:

* Re-import the binary into Ghidra but firstly note the information in the `Language` box - our binary is an x86 64-bit binary in Little Endian format, compiled with gcc.

![language](images/language.png)

* Change the `Format` drop-down box from `ELF` to `Raw Binary`. This will clear the `Language` box.
* Hit the browse button next to the `Language` box and scroll down to find the find the binary type identified in step 1 above. I had to find the x86 64-bit gcc option.

![language2](images/language2.png)

* Import the binary.

![import](images/import.png)

Importing a raw binary into Ghidra loses a lot of information, including function names. By comparing the contents of the functions we can see that `print_flag` has been renamed to `FUN_00001261`. That name is descriptive because by looking at the listing pane we can see that the function starts at memory address 0x001261. We can find the same information using `objdump -d patches | grep print_flag`. This is the memory address that we want to divert `main` to. 

We will achieve this by changing the call to `puts` (0x001030) to 0x001261. To patch the binary:

1. Select the call to puts - at address 0x12e4: `CALL FUN_00001030`
1. `CTRL+SHIFT+g` to edit (or right click, `Patch Instruction`)
1. Change `0x00001030` read `0x00001261` 
1. Press `o` to output the file (or `file` > `export program`)
1. Change the output format to `binary`
1. Save it
1. Return to the terminal and run the new program.

![flag](images/flag.png)

---

### Flag 
```
nactf{unl0ck_s3cr3t_funct10n4l1ty_w1th_b1n4ry_p4tch1ng_L9fcKhyPupGVfCMZ}
```
