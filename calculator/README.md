## Calculator

* **CTF:** Newark Academy CTF 2020
* **Category:** Web
* **Points:**  150
* **Author(s):** b0uldrr
* **Date:** 1/11/20

---

### Challenge
```
Kevin has created a cool calculator that can perform almost any mathematical operation! It seems that he might have done this the lazy way though... He's also hidden a flag variable somewhere in the code.

https://calculator.challenges.nactf.com/

-izhang05
```
---

### Hint
```
What's the easiest way to evaluate user input?
```
---

### Solution
The challenge website is a calculator which works by passing user input directly to the php function `eval()`. Because the input isn't sanitised, we can use `system` calls print the source of the index.php file.

* `system("whoami") returned our user as `www-data`
* `system("ls") returned a single filename, `index.php`. This is useful because we know that the flag is somewhere in the source. If we can print the source then we can get the flag.
* `system("cat index.php | grep nact")` returned the flag.

![flag](images/flag.png)
---

### Flag 
```
nactf{ev1l_eval}
```
