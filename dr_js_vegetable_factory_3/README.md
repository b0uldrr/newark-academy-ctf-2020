## Dr. J's Vegetable Factory #3

* **CTF:** Newark Academy CTF 2020
* **Category:** General Skills
* **Points:** 173
* **Author(s):** b0uldrr
* **Date:** 2/11/20

---

### Challenge
```
Rahul hates vegetables. Rahul hates vegetables so much that he snuck into Dr. J's factory at night to sabotage Dr. J's vegetable production! He brought a sledgehammer and broke the wheels of Dr. J's robot! 😓 Now the robot is stuck in place, and instead of being able to swap any adjacent elements, it can only swap the elements in positions 0 and 1!

But Dr. J won't let this incident stop him from giving the people the vegetables they deserve! Dr. J is a problem solver 🧠. He organized his vegetables in a circle, and added a conveyor-belt that allows him shift the positions of the vegetables. He thinks that the conveyor belt should make it possible to sort his vegetables, but he's not 100% sure. Can you help him out?

nc challenges.ctfd.io 30267

Enter letters separated by spaces to sort Dr. J's vegetables. Entering "c" will activate the conveyor belt and shift all vegetables left one position. Entering "s" will swap the vegetable in position 0 with the vegetable in position 1.

The20thDuck

```
---

### Hint
```
This problem boils down to the same thing as #2. Try bubble sort!
```
---

### Solution

Connecting to the challenge server, we are presented with an unordered list of vegetables that we need to sort. As per the hint, my solution was very similar to [bubble sort](https://en.wikipedia.org/wiki/Bubble_sort).

![problem](images/problem.png)

```python
#!/usr/bin/python3

import pwn
payload = ""

# Swap positions 1 and 0 in the list
def swap():
    global payload, veg
    veg[0], veg[1] = veg[1], veg[0]
    payload = payload + " s"

# Rotate the entire list one position left, appending the item 0 to the end
def rotate():
    global payload, veg
    veg.append(veg.pop(0))
    payload = payload + " c"


conn = pwn.remote("challenges.ctfd.io", 30267)
conn.recvuntil("Enter 1, 2, 3, 4, or 5.\n")
conn.sendline("3")

# Discard the first 4 lines which are instructions/blank
for i in range(0, 4):
    conn.recvline()

# Receive the vegetables line, tidy it up and put it into a list
veg = conn.recvline().decode().strip().split(', ')

# Algorithm is similar to bubble sort, just with some modifications due to
# the limitations of the conveyor system.
veg_len= len(veg)
count = 0
for i in range(0, veg_len):

    for j in range(0, count):
        rotate()

    for j in range(0, veg_len - count - 1):
        if veg[0] > veg[1]:
            swap()
        rotate()

    count = count + 1

# Send our payload and receive the output/flag
conn.sendline(payload)
print(conn.recvall().decode())
```

![flag](images/flag.png)

---

### Flag 
```
nactf{1t_t4k35_tw0_t0_m4n90_8a51c7b47fbe227}
```
