#!/usr/bin/python3

import pwn
payload = ""

# Swap positions 1 and 0 in the list
def swap():
    global payload, veg
    veg[0], veg[1] = veg[1], veg[0]
    payload = payload + " s"

# Rotate the entire list one position left, appending the item 0 to the end
def rotate():
    global payload, veg
    veg.append(veg.pop(0))
    payload = payload + " c"


conn = pwn.remote("challenges.ctfd.io", 30267)
conn.recvuntil("Enter 1, 2, 3, 4, or 5.\n")
conn.sendline("3")

# Discard the first 4 lines which are instructions/blank
for i in range(0, 4):
    conn.recvline()

# Receive the vegetables line, tidy it up and put it into a list
veg = conn.recvline().decode().strip().split(', ')

# Algorithm is similar to bubble sort, just with some modifications due to
# the limitations of the conveyor system.
veg_len= len(veg)
count = 0
for i in range(0, veg_len):

    for j in range(0, count):
        rotate()

    for j in range(0, veg_len - count - 1):
        if veg[0] > veg[1]:
            swap()
        rotate()

    count = count + 1

# Send our payload and receive the output/flag
conn.sendline(payload)
print(conn.recvall().decode())


