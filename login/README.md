## Login

* **CTF:** Newark Academy CTF 2020
* **Category:** Web 
* **Points:** 175
* **Author(s):** b0uldrr
* **Date:** 01/11/20 

---

### Challenge
```
Vyom has learned his lesson about client side authentication and his soggy croutons will now be protected. There's no way you can log in to this secure portal!

https://login.challenges.nactf.com/

-izhang05

```
---

### Hint
```
https://xkcd.com/327/
```
---

### Solution
The challenge site is a login page, vulnerable to sql injection.

![sql](images/sql.png)
![flag](images/flag.png)

---

### Flag 
```
nactf{sQllllllll_1m5qpr8x}

```
