## Newark Academy CTF 2020

* **CTF Time page:** https://ctftime.org/event/1157
* **Category:** Jeopardy
* **Date:** Fri, 30 Oct. 2020, 22:00 UTC — Wed, 04 Nov. 2020, 21:59 UTC

---

### Solved Challenges
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
| Calculator | web | 150 | php, exec|
| Cookie Recipie | web | 150 | cookies |
| Dr J's Vegetable Factory #3 | general | 175 | prog, bubble sort|
| dROPit | pwn | 300 | bof, rop, ret2libc, 64 bit|
| Error 0 | crypto | 150 | prog |
| Generic Flag Checker 1 | rev | 75 | static analysis, ghidra, strings |
| Generic Flag Checker 2 | rev | 150 | dynamic analysis, gdb, ltrace |
| Greeter | pwn | 150 | bof |
| Login | web | 175 | sql injection |
| Patches | rev | 150 | binary patching, Ghidra, gdb | 
| World Trip | general | 300 | prog |

### Unsolved - Revisit for Practice

| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
| Format | pwn | 300 | format strings, printf |
