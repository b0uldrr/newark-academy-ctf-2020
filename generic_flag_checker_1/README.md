## Generic Flag Checker 1

* **Category:** Rev
* **Points:** 75 

### Challenge
```
Flag Checker Industries™ has released their new product, the Generic Flag Checker®! Aimed at being small, this hand-assembled executable checks your flag in only 8.5kB! Grab yours today!

-asphyxia

```
---

### Downloads
* [gfc1](gfc1)

---

### Solution
* **Author(s):** b0uldrr
* **Date:** 31/10/20

Opened the provided binary in Ghidra. The flag was in in plaintext in the `entry()` function. I over-thought this one... the flag is visible with `strings`.

![flag](images/flag.png)

### Flag 
```
nactf{un10ck_th3_s3cr3t5_w1th1n_cJfnX3Ly4DxoWd5g}
```
