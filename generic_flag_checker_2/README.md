## Generic Flag Checker 2

* **Category:** Rev
* **Points:** 150 
* **Author(s):** b0uldrr
* **Date:** 31/10/20

### Challenge
```
Flag Checker Industries™ is back with another new product, the Generic Flag Checker® Version 2℠! This time, the flag has got some foolproof math magic preventing pesky players from getting the flag so easily.

-asphyxia

```

### Hint
```
When static analysis fails, maybe you should turn to something else...
```

### Downloads
* [gfc2](gfc2)

### Solution
*Note: The below solution works, and it's good to have an understanding of using GDB to examine the stack, but in reading other writeups, you can just use `ltrace`. When ./gfc2 compares our input to the flag, it does that with a call to strncmp(), which is a libc function. `ltrace` will show this in its output. Note that by default `ltrace` will only show 32 stirng characters, so you need to specify more with `ltrace -s 56 ./gfc2`.*

Stripped binary. Nothing helpful in Ghidra.

[Found this website](https://medium.com/@tr0id/working-with-stripped-binaries-in-gdb-cacacd7d5a33) with help on using GDB with stripped binaries. Specifically, I used `file info` in GDB to find the entry point.

* Open file in GDB
* Run it once to load addresses into memory
* `info file` to find entry point
* `0x5555555552b0` to set a breakpoint at the identified entry
* Continue to step through the program with `s` until we reach the point where we enter our input.
* Continue to step through the program to see how it compares our input with the actual flag value.
* We can see after some time that the actual flag value is being calculated and stored on the stack for comparison. This shows up in GDB-Peda in the stack snapshot after each instruction, but you can examine the stack specifically with `x/100wx $rsp`, or, `x/s 0x7fffffffe050`.

![flag](images/flag.png)
![flag2](images/flag2.png)

### Flag 
```
nactf{s0m3t1m3s_dyn4m1c_4n4lys1s_w1n5_gKSz3g6RiFGkskXx}
```
