## Cookie Recipe

* **CTF:** Newark Academy CTF 2020
* **Category:** Web
* **Points:** 150
* **Author(s):** b0uldrr
* **Date:** 31/10/20

---

### Challenge
```
Arjun owns a cookie shop serving warm, delicious, oven-baked cookies. He sent me his ages-old family recipe dating back four generations through this link, but, for some reason, I can't get the recipe. Only cookie lovers are allowed!

https://cookies.challenges.nactf.com/index.php

-izhang05
```
---

### Hint
```
Arjun baked a cookie as an offering, but he accidently placed it on the front page.
```
---

### Solution

Visiting the challenge site, we are presented with a simple login page.

![landing](images/landing.png)

Any username/password combo will successfully log you in and take you to `auth.php`, but you are presented with a message that "you're not a cookie lover!"

![unsuccessful](images/unsuccessful.png)

The site sets a cookie when you first visit. The cookie value is set to `cookie_lover` but the path is only set to `index.php`. Changing this to `auth.php` will allow us to successfully login and see the flag.

![flag](images/flag.png)

---

### Flag 
```
nactf{c00kie_m0nst3r_5bxr16o0z} 
```
